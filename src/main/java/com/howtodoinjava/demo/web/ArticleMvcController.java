package com.howtodoinjava.demo.web;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.howtodoinjava.demo.exception.RecordNotFoundException;
import com.howtodoinjava.demo.model.ArticleEntity;
import com.howtodoinjava.demo.service.ArticleService;

@Controller
@RequestMapping("/")
public class ArticleMvcController
{
	@Autowired
	ArticleService service;

	@RequestMapping
	public String getAllArticles(Model model)
	{
		List<ArticleEntity> list = service.getAllArticle();

		model.addAttribute("articles", list);
		return "list-articles";
	}

	@RequestMapping(path = {"/edit", "/edit/{id}"})
	public String editArticleById(Model model, @PathVariable("id") Optional<Long> id)
							throws RecordNotFoundException
	{
		if (id.isPresent()) {
			ArticleEntity entity = service.getArticleById(id.get());
			model.addAttribute("article", entity);
		} else {
			model.addAttribute("article", new ArticleEntity());
		}
		return "add-edit-article";
	}
	
	@RequestMapping(path = "/delete/{id}")
	public String deleteArticleById(Model model, @PathVariable("id") Long id)
							throws RecordNotFoundException 
	{
		service.deleteArticleById(id);
		return "redirect:/";
	}

	@RequestMapping(path = "/createArticle", method = RequestMethod.POST)
	public String createOrUpdateArticle(ArticleEntity articleEntity)
	{
		service.createOrUpdateArticle(articleEntity);
		return "redirect:/";
	}
}
