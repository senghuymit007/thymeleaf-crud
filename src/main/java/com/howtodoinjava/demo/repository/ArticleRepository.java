package com.howtodoinjava.demo.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.howtodoinjava.demo.model.ArticleEntity;

@Repository
public interface ArticleRepository
			extends CrudRepository<ArticleEntity, Long> {

}
